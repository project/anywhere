<?php

/**
 * @file
 * Displays a Tweet Box to allow visitors to post statuses to Twitter.
 *
 * Available variables:
 * - $counter: Boolean specifying if a dynamic character counter should be visiable.
 * - $height: Height of the Tweet Box in pixals.
 * - $width: Width of the Tweet Box pixals.
 * - $label: The text prompt shown above the Tweet Box.
 * - $default_content: Default text to go in the Tweet Box.
 *
 * @ingroup themeable
 */
 ?>

<span id="anywhere-block-tweet-box"></span>
<script type="text/javascript">
  twttr.anywhere("<?php echo $api_version; ?>", function (twitter) {

    twitter("#anywhere-block-tweet-box").tweetBox({
      counter: <?php echo $counter; ?>,
      height: <?php echo $height; ?>,
      width: <?php echo $width; ?>,
      label: "<?php echo $label; ?>",
      defaultContent: "<?php echo $default_content; ?>"
    });

  });
</script>