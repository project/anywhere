<?php

/**
 * @file
 * The admin form for configuring the anywhere module.
 *
 */

/**
 * Implementation of hook_admin().
 */
function anywhere_admin() {
  $form = array();

  $form['anywhere_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('anywhere_api_key', ''),
    '#description' => t('Get an API key by registering your application with <a href="http://dev.twitter.com/anywhere/apps/new" target="_blank">Twitter</a>.'),
    '#required' => TRUE,
  );
  $form['anywhere_api_version'] = array(
    '#type' => 'textfield',
    '#title' => t('API version'),
    '#default_value' => variable_get('anywhere_api_version', '1'),
    '#description' => t("The @Anywhere API version. Currently only version 1 is available."),
    '#required' => TRUE,
  );
  $form['anywhere_link_type'] = array(
    '#type' => 'radios',
    '#title' => t('Link type'),
    '#description' => t('What type of auto link do you want? Read about the different types on <a href="http://dev.twitter.com/anywhere/begin" target="_blank">Twitter</a>.'),
    '#options' => array(t('None'), t('Auto-linkification of @usernames'), t('Hoovercards')),
    '#default_value' => variable_get('anywhere_link_type', 2),
    '#required' => TRUE,
  );
  $form['anywhere_link_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Link DOM selector (Coming soon)'),
    '#default_value' => variable_get('anywhere_link_selector', ''),
    '#description' => t('@Anywhere supports <a href="http://api.jquery.com/category/selectors/" target="_blank">jQuery</a> DOM selectors to only link specific classes, elements, etc.'),
    '#attributes' => array('disabled' => 'disabled'),
  );

  return system_settings_form($form);
}