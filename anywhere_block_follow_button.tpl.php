<?php

/**
 * @file
 * Displays a button to allow visitors to follow a specific Twitter user.
 *
 * Available variables:
 * - $screen_name: The screen_name of the Twitter account to be followed.
 *
 * @ingroup themeable
 */
 ?>
<?php if (empty($screen_name)) { ?>
  The follow button block is not <?php echo l(t('configured'), 'admin/build/block/configure/anywhere/anywhere-block-follow-button'); ?>.
<?php
}
else {
?>
  <div id="anywhere-block-follow-button"></div>
  <script type="text/javascript">
  	twttr.anywhere(function(twitter) {
    	twitter('#anywhere-block-follow-button').followButton("<?php echo $screen_name; ?>")
  		})
  </script>
<?php
}
